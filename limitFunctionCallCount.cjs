const counterFactory = require('./counterFactory.cjs');

function limitFunctionCallCount(callBackFunction, number) {
    let countCallBackFunction = 0;
    number = parseInt(number);
    if(typeof callBackFunction === 'function' && !Number.isNaN(number)){
        return function innerCallBackFunction(...argument){
            if(countCallBackFunction < number){
                countCallBackFunction = counterFactory(countCallBackFunction).increment();
                return callBackFunction(...argument);
            }
            else{
                return null;
            }
        }
    }
    else{
        throw new Error("Incorrect Arguments passed");
    }
    
}
module.exports = limitFunctionCallCount;