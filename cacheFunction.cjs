function cacheFunction(callBackFunction){
    const argumentsCache = {};
    if(typeof callBackFunction === 'function'){
        return function innerCacheFunction(...argument){
            if(argumentsCache.hasOwnProperty(argument)){
                return argumentsCache[argument];
            }
            else{
                const callBackFunctionResult = callBackFunction(...argument);
                argumentsCache[argument] = callBackFunctionResult;
                return callBackFunctionResult;
            }
        }
    }
    else{
        throw new Error("Incorrect arguments passed to cacheFunction")
    }
}

module.exports = cacheFunction;