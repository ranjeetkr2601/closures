let cacheFunction = require('../cacheFunction.cjs');

function callBackFunction(...argument){
    let sum = argument.reduce((acc, val) => {
        acc = acc + val;
        return acc;
    },0);
    return sum;
}
try{
    const innerCacheFunction = cacheFunction(callBackFunction);
    innerCacheFunction(1,2,3);
    innerCacheFunction(10);
    console.log(innerCacheFunction(1,3,2));
    console.log(innerCacheFunction(1,2,3));
}
catch(error){
    console.error(error);
}