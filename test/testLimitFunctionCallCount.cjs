let limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

let maxCount = 4;
let callBackFunction = function(){
    return "Successfully invoked callBackFunction";   
}
try{
    let innerCallBackFunction = limitFunctionCallCount(callBackFunction, maxCount);
    for(let count = 0; count < maxCount + 1; count++){
        console.log(innerCallBackFunction());
    }
}
catch(error){
    console.error(error);
}