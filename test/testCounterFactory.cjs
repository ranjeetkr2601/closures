const counterFactory = require('../counterFactory.cjs');

const result = counterFactory();
console.log(result.increment(0));
console.log(result.decrement(1));

const ans = counterFactory(1);
console.log(ans.increment(0));