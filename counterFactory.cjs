function counterFactory(counter = 0){
    if(Number.isNaN(counter)){
        counter = 0;
    }
    return {
        increment : function (value = 1) {
            if(!Number.isNaN(value)){
                counter = counter + value;
            }
            return counter;
        },
        decrement : function (value = 1) {
            if(!Number.isNaN(value)){
                counter = counter - value;
            }
            return counter;
        }
    }
}
module.exports = counterFactory;